package com.inn.cafe.constants;

public class CafeConstants {

    //this class was created to insert values that will not change during
    //the execution of the program
    public static final String SOMETHING_WENT_WRONG="Something went wrong!";
    public static final String INVALID_DATA= "Invalid Data!";

    public static final String UNAUTHORIZED_ACCESS= "Unauthorized access!";

    public static final String STORE_LOCATION= "C:\\Users\\Lenovo\\Documents\\cafeStoredFiles";
}
