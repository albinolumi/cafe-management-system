package com.inn.cafe.JWT;

public final class JwtConstants {
    private JwtConstants() {
    }

    public static final String BEARER_PREFIX = "Bearer ";
}
